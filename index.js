async function fetchData(offsetData = 0, limitData = 4) {
	let dataList = ""

	let response = await fetch('https://api.spacexdata.com/v4/launches/query',
	{
		method: 'POST',
		headers: {
		  'Content-Type': 'application/json'
		},
		body: JSON.stringify({
		  "options": {
		      "limit": limitData,
		      "offset": offsetData
		  }
		})
	})
	.then((response) => response.json())
	.then((data) => {
		for(let i = 0; i < data['docs'].length; i++){
			let template = 
			'<div class="card mb-3" style="max-width: 540px;">'+
  				'<div class="row no-gutters">' +
  				 	'<div class="col-md-4">' +
     				 	'<img src="./images/img.png" alt="..." class="img-fluid">' +
   				 	'</div>' +
   				 '<div class="col-md-8">' +
      				'<div class="card-body">' +
     					'<h5 class="card-title">Flight Number: '+ data['docs'][i].flight_number +' '+ data['docs'][i].name + ' ' +
     					'('+ year(data['docs'][i].date_local)+')</h5>' +
       					'<p class="card-text">Details: '+ data['docs'][i].details +'</p>'+
      				'</div>' +
    			'</div>' +
  				'</div>' +
			'</div>'
			dataList += template
		}

		$('.card-container').append(dataList)
	})
}

fetchData()

let year = (date) => {
	let splitDate = date.split('-')
	return splitDate[0]
}

let offsetData = 0
let limitData = 4
$(window).scroll(function() {
  	if($(window).scrollTop() + $(window).height() == $(document).height()) {
  		$('.loader').removeClass('d-none')
  	offsetData += 4
  	limitData += 4
    fetchData(offsetData, limitData);
    	$('.loader').addClass('d-none')
  }
});